import request from '../../mock/requests/productsRequest';

export class ProductsRequest {

    #productsRepository;
    #productsFactory;

    constructor(
        productsRepository,
        productsFactory
    ) {
        this.#productsRepository = productsRepository;
        this.#productsFactory = productsFactory;
    }

    async fetch(url) {
        return request(url).then((data) => {
            this.#productsRepository.setProducts(
                this.#productsFactory.createCollection(data)
            )
        })
    }
}
