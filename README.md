# Discount Calculation Story
**To read**: [<link to story opened in Refactories Story UI, just link from browser is good enough>]

**To code yourself**: [https://git.epam.com/anatolii_kravchenko/discount-calculation-story-js]

**Estimated reading time**: 40 minutes

## Story Outline
This story contains useful information about DIP (Dependency Inversion Principle) from the First 5 Principles of Object Oriented Design - SOLID. Here you can also find explanation how to implement this principle though the abstraction and three ways of dependency injection.

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_design, #javascript, #dependency_inversion_principle #solid #dependency_in
